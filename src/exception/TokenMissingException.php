<?php

namespace dengje\jwt\exception;

class TokenMissingException extends JWTException
{
    protected $message = 'token missing';
}
