<?php

namespace dengje\jwt\claim;

class Audience extends Claim
{
    protected $name = 'aud';
}
