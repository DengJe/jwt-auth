<?php


namespace dengje\jwt;

use dengje\jwt\claim\Factory;
use dengje\jwt\claim\Issuer;
use dengje\jwt\claim\Audience;
use dengje\jwt\claim\Expiration;
use dengje\jwt\claim\IssuedAt;
use dengje\jwt\claim\JwtId;
use dengje\jwt\claim\NotBefore;
use dengje\jwt\claim\Subject;

class Payload
{
    protected $factory;

    protected $classMap
        = [
            'aud' => Audience::class,
            'exp' => Expiration::class,
            'iat' => IssuedAt::class,
            'iss' => Issuer::class,
            'jti' => JwtId::class,
            'nbf' => NotBefore::class,
            'sub' => Subject::class,
        ];

    /**
     * @var array
     */
    public const CLAIMS_MAP = [
        'aud' => 'permittedFor',
        'exp' => 'expiresAt',
        'iat' => 'issuedAt',
        'iss' => 'issuedBy',
        'jti' => 'identifiedBy',
        'nbf' => 'canOnlyBeUsedAfter',
        'sub' => 'relatedTo',
    ];

    protected $claims;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function customer(array $claim = [])
    {
        foreach ($claim as $key => $value) {
            $this->factory->customer(
                $key,
                is_object($value) ? $value->getValue() : $value
            );
        }

        return $this;
    }

    public function get()
    {
        $claim = $this->factory->builder()->getClaims();

        return $claim;
    }

    public function check($refresh = false)
    {
        $this->factory->validate($refresh);

        return $this;
    }


    /**
     * @desc match class map
     * @param string $key
     * @return string|null
     */
    public function matchClassMap(string $key): ?string
    {
        $class = null;
        switch($key) {
            case 'aud':
                $class = Audience::class;
                break;
            case 'exp':
                $class = Expiration::class;
                break;
            case 'iat':
                $class = IssuedAt::class;
                break;
            case 'iss':
                $class = Issuer::class;
                break;
            case 'jti':
                $class = JwtId::class;
                break;
            case 'nbf':
                $class = NotBefore::class;
                break;
            case 'sub':
                $class = Subject::class;
                break;
        }

        return $class;
    }
}
