<?php


namespace dengje\jwt;

use dengje\jwt\command\SecretCommand;
use dengje\jwt\middleware\InjectJwt;
use dengje\jwt\provider\JWT as JWTProvider;

class Service extends \think\Service
{
    public function boot()
    {
        $this->commands(SecretCommand::class);

        (new JWTProvider())->register();
    }
}
